<?php 
	session_start();
	if(isset($_SESSION['extrasensories']))
		$extrasensories = $_SESSION['extrasensories'];
	else{
		$extrasensories = array();
		$_SESSION['extrasensories'] = $extrasensories;
	}
	if(isset($_SESSION['userNumbers'])){
		$userNumbersAll = $_SESSION['userNumbers'];
	}
	else{
		$userNumbersAll = array();
	}
	$countExtrasensories = count($extrasensories);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Тестирование экстрасенсов</title>
		<link rel="stylesheet" type="text/css" href="css/materialize.min.css" >
		<link rel="stylesheet" type="text/css" href="css/testExtrasensory.css" >
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="js/materialize.min.js"></script>
		<script src="js/testExtrasensory.js"></script>
	</head>
	<body>
    <nav>
        <div class="nav-wrapper blue">
            <a href="#" class="brand-logo">Extrasensory</a>
        </div>
    </nav>
    <main>
        <div class="row">
            <div class="col s10 offset-s1">
                <div class="main-block" id="no-extrasensories" <?php if($countExtrasensories >= 2) echo "style='display:none;'"?>>
                    <h3>Экстрасенсы еще не пришли. Вы можете позвать нескольких.</h3>
                    <p>Для верного тестирования необходимо 2 и более экстрасенсов. Для Вашего удобства рекомендуем тестировать не более 10 экстрасенсов одновременно.</p>
                    <div>Позвать <input type="number" id="extrasensory-count" min="2"> экстрасенсов <button class="btn-flat blue" id="call-extrasensory-start" onclick="callExtrasensories()">Позвать</button><span class="hide" id="preloader"><img src="img/preloader.gif"></span>
                    </div>

                </div>
                <div class="main-block" id="make-number" <?php if($countExtrasensories < 2) echo "style='display:none;'"?>>
                    <h3>Экстрасенсы готовы к тестированию</h3>
                    <div id="printed-numbers">
                    <?php if($userNumbersAll):?>
                        <p>Вы уже вводили числа:
                            <?php foreach($userNumbersAll as $number) echo $number.' ' ?>
                        </p>
                    <?php endif ?>
                    </div>
                    <div>Загадайте двухзначное число. Когда будете готовы, нажмите <button class="btn-flat blue" onclick="predictValues()">на кнопку</button></div>
                </div>
                <div class="main-block" id="results" style='display:none;'>
                    <h3>Варианты от экстрасенсов:</h3>
                    <table class="main-table" border="1" id="extrasensory-results">
                        <thead>
                            <tr>
                                <th>Имя экстрасенса</th>
                                <th>Ответ</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div>Введите загаданное число <input type="number" id="user-number" min="10" max="99" >
                    <button class="btn-flat blue" onclick="validateNumber()">Отправить</button></div>
                </div>
                <div class="main-block" id="statistics" style='display:none;'>
                    <h3>Ваш ответ получен. Статистика по экстрасенсам:</h3>
                    <div id="extrasensory-stats">

                    </div>
                    <button class="btn-flat blue" class="try-again-button" onclick="tryAgain()">Попробовать еще раз</button>
                </div>
            </div>
        </div>
    </main>
    <footer class="page-footer blue">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Пример кода</h5>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2020 Sboev S.
            </div>
        </div>
    </footer>
	</body>
<html>